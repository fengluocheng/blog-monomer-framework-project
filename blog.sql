/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50528
Source Host           : localhost:3306
Source Database       : blog

Target Server Type    : MYSQL
Target Server Version : 50528
File Encoding         : 65001

Date: 2022-12-15 19:53:30
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for blog_article
-- ----------------------------
DROP TABLE IF EXISTS `blog_article`;
CREATE TABLE `blog_article` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `article_pic` varchar(255) DEFAULT NULL COMMENT '文章封面',
  `article_name` varchar(255) DEFAULT NULL COMMENT '文章名称',
  `article_tag` varchar(255) DEFAULT NULL COMMENT '文章标签',
  `article_remark` varchar(255) DEFAULT NULL COMMENT '文章简介',
  `article_read_count` int(11) DEFAULT '0' COMMENT '文章阅读量',
  `article_state` int(1) DEFAULT '0' COMMENT '文章状态(0:草稿;1:发布;2:弃用)',
  `user_id` bigint(11) DEFAULT NULL COMMENT '作者ID',
  `article_content` text COMMENT '文章内容',
  `update_time` datetime DEFAULT NULL COMMENT '最后更新时间',
  `article_type` bigint(11) DEFAULT NULL COMMENT '文章类型',
  `article_star_num` int(11) DEFAULT '0' COMMENT '文章点赞数',
  `article_comment_num` int(11) DEFAULT '0' COMMENT '文章评论数',
  `top_state` int(1) DEFAULT NULL COMMENT '是否置顶(0:不置顶;1:置顶)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8 COMMENT='文章信息表';

-- ----------------------------
-- Records of blog_article
-- ----------------------------
INSERT INTO `blog_article` VALUES ('1', '', 'final关键字的作用', 'java,java基础', '今天我们一起来学习一下final关键字的作用', '3', '1', '12', '<p>final关键字的作用很大，一定要会用</p>', '2022-09-24 14:11:18', '1', '0', '0', '0');
INSERT INTO `blog_article` VALUES ('83', 'https://liziao1.oss-cn-hangzhou.aliyuncs.com/exampledir/f01904b8-176f-4ddf-ba4b-d98e5ba2e963.png', 'brbrbw', 'wevewvwev', null, '0', '1', '12', null, '2022-09-25 17:31:00', '3', '0', '0', '0');
INSERT INTO `blog_article` VALUES ('84', null, 'v為v為v', 'vewe', null, '0', '1', '12', null, '2022-09-26 16:25:10', '3', '0', '0', '0');

-- ----------------------------
-- Table structure for blog_article_type
-- ----------------------------
DROP TABLE IF EXISTS `blog_article_type`;
CREATE TABLE `blog_article_type` (
  `id` bigint(5) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `type_name` varchar(50) DEFAULT NULL COMMENT '文章类型名称',
  `status` int(2) DEFAULT NULL COMMENT '状态(0:不可用;1:可用)',
  `update_user_name` varchar(20) DEFAULT NULL COMMENT '最后更新人名称',
  `update_time` datetime DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='文章类型表';

-- ----------------------------
-- Records of blog_article_type
-- ----------------------------
INSERT INTO `blog_article_type` VALUES ('1', 'Java基础', '1', '吴大胖', '2022-03-01 13:24:31');
INSERT INTO `blog_article_type` VALUES ('2', 'Mysql', '1', '吴大胖', '2022-03-01 13:27:40');
INSERT INTO `blog_article_type` VALUES ('3', '面试宝典', '1', '吴大胖', '2022-03-01 16:40:51');
INSERT INTO `blog_article_type` VALUES ('4', 'SpringBoot', '1', '吴大胖', '2022-03-01 16:40:56');
INSERT INTO `blog_article_type` VALUES ('5', 'SpringCloud', '1', '吴大胖', '2022-03-01 16:41:05');
INSERT INTO `blog_article_type` VALUES ('6', 'SpringMVC', '1', '吴大胖', '2022-03-01 16:41:10');
INSERT INTO `blog_article_type` VALUES ('7', 'Mybatis', '1', '吴大胖', '2022-03-01 16:42:20');
INSERT INTO `blog_article_type` VALUES ('8', 'Dubbo', '1', '吴大胖', '2022-03-01 16:42:24');
INSERT INTO `blog_article_type` VALUES ('9', 'Nacos', '1', '吴大胖', '2022-03-01 16:42:29');
INSERT INTO `blog_article_type` VALUES ('10', '算法题', '1', '吴大胖', '2022-03-01 16:42:40');
INSERT INTO `blog_article_type` VALUES ('11', 'Redis', '1', '吴大胖', '2022-03-03 13:09:38');
INSERT INTO `blog_article_type` VALUES ('12', 'JVM', '1', '吴大胖', '2022-03-03 13:10:54');
INSERT INTO `blog_article_type` VALUES ('13', '武智睿的小黄书', '1', null, null);

-- ----------------------------
-- Table structure for blog_friend_address
-- ----------------------------
DROP TABLE IF EXISTS `blog_friend_address`;
CREATE TABLE `blog_friend_address` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(50) DEFAULT NULL COMMENT '链接名称',
  `address` varchar(500) DEFAULT NULL COMMENT '链接地址',
  `update_user_name` varchar(20) DEFAULT NULL COMMENT '最后更新人名称',
  `update_time` datetime DEFAULT NULL COMMENT '最后更新时间',
  `status` int(1) DEFAULT NULL COMMENT '状态(0:不可用;1:可用)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='友情链接表';

-- ----------------------------
-- Records of blog_friend_address
-- ----------------------------
INSERT INTO `blog_friend_address` VALUES ('2', 'RBAC权限校验实战', 'https://blog.csdn.net/wujiangbo520/article/details/122636877', '吴大胖', '2022-02-28 16:31:31', '1');
INSERT INTO `blog_friend_address` VALUES ('3', '简单易懂的单例模式', 'https://blog.csdn.net/wujiangbo520/article/details/122059561', '吴大胖', '2022-02-28 16:31:34', '1');

-- ----------------------------
-- Table structure for blog_param
-- ----------------------------
DROP TABLE IF EXISTS `blog_param`;
CREATE TABLE `blog_param` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `update_user_name` varchar(20) DEFAULT NULL COMMENT '最后修改人名称',
  `update_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  `param_key` varchar(50) DEFAULT NULL COMMENT '参数键',
  `param_value` varchar(200) DEFAULT NULL COMMENT '参数值',
  `param_desc` varchar(100) DEFAULT NULL COMMENT '参数描述',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='博客参数信息表';

-- ----------------------------
-- Records of blog_param
-- ----------------------------
INSERT INTO `blog_param` VALUES ('1', '吴大胖', '2022-03-01 19:26:27', 'user_weixin_url', 'https://hrm0620.oss-cn-chengdu.aliyuncs.com/wx.jpg', '博主微信二维码名片');
INSERT INTO `blog_param` VALUES ('2', '吴大胖', '2022-03-01 19:27:47', 'user_weixin_no', '1134135987', '博主的QQ账号');
INSERT INTO `blog_param` VALUES ('3', '吴大胖', '2022-03-01 19:28:13', 'user_email', '1134135987@qq.com', '博主的邮箱地址');
INSERT INTO `blog_param` VALUES ('4', '吴大胖', '2022-03-02 13:22:00', 'ad_url', 'https://www.aliyun.com/daily-act/ecs/activity_selection?userCode=1vvv9yks', '点击广告栏位后跳转地址');
INSERT INTO `blog_param` VALUES ('5', '吴大胖', '2022-03-02 13:21:46', 'ad_1001_url', 'https://hrm0620.oss-cn-chengdu.aliyuncs.com/ad_1001.png', '广告栏位1的图片地址');
INSERT INTO `blog_param` VALUES ('6', '吴大胖', '2022-03-02 13:22:26', 'ad_1002_url', 'https://hrm0620.oss-cn-chengdu.aliyuncs.com/ad_1002.png', '广告栏位2的图片地址');
INSERT INTO `blog_param` VALUES ('8', null, '2022-09-26 11:24:40', 'evve', 'verv', 'rver');

-- ----------------------------
-- Table structure for cp
-- ----------------------------
DROP TABLE IF EXISTS `cp`;
CREATE TABLE `cp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cp_name` varchar(255) DEFAULT NULL COMMENT '菜品名称',
  `cp_src` varchar(255) DEFAULT NULL COMMENT '菜品图片',
  `cp_wd` varchar(255) DEFAULT NULL COMMENT '菜品味道',
  `cp_price` decimal(10,2) DEFAULT NULL COMMENT '菜品价格',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '最近一次上菜时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of cp
-- ----------------------------
INSERT INTO `cp` VALUES ('1', '青椒肉丝', 'https://bendong.oss-cn-hangzhou.aliyuncs.com/baictup.jpg', '中辣', '19.20', '2022-09-07 21:49:24', '2022-09-07 21:49:27');
INSERT INTO `cp` VALUES ('2', '卡布奇洛', 'https://bendong.oss-cn-hangzhou.aliyuncs.com/kbql.jpg', '不辣', '20.30', '2022-09-07 21:50:15', '2022-09-07 21:50:17');
INSERT INTO `cp` VALUES ('3', '红烧牛肉', 'https://bendong.oss-cn-hangzhou.aliyuncs.com/bcxg.jpeg', '中辣', '21.50', '2022-09-06 21:52:03', '2022-09-08 21:52:06');
INSERT INTO `cp` VALUES ('12', '6', null, '重辣', '29.00', null, '2022-09-29 12:01:45');

-- ----------------------------
-- Table structure for cp_login_line
-- ----------------------------
DROP TABLE IF EXISTS `cp_login_line`;
CREATE TABLE `cp_login_line` (
  `cp_id` int(11) DEFAULT NULL COMMENT '菜品id',
  `login_id` int(11) DEFAULT NULL COMMENT '用户id',
  `create_time` datetime DEFAULT NULL,
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of cp_login_line
-- ----------------------------
INSERT INTO `cp_login_line` VALUES ('1', '12', null, '12');
INSERT INTO `cp_login_line` VALUES ('2', null, '2022-09-28 19:07:27', '15');
INSERT INTO `cp_login_line` VALUES ('1', null, '2022-09-28 19:10:35', '19');
INSERT INTO `cp_login_line` VALUES ('2', null, '2022-09-28 20:11:29', '21');
INSERT INTO `cp_login_line` VALUES ('2', null, '2022-09-28 20:24:19', '23');
INSERT INTO `cp_login_line` VALUES ('3', null, '2022-09-29 10:27:57', '25');
INSERT INTO `cp_login_line` VALUES ('1', null, '2022-09-29 10:45:28', '26');
INSERT INTO `cp_login_line` VALUES ('1', null, '2022-10-10 20:34:59', '27');
INSERT INTO `cp_login_line` VALUES ('2', null, '2022-10-10 20:35:36', '28');
INSERT INTO `cp_login_line` VALUES ('3', null, '2022-10-10 20:38:19', '29');

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `real_name` varchar(10) DEFAULT NULL COMMENT '操作人姓名',
  `content` varchar(500) DEFAULT NULL COMMENT '日志内容',
  `create_time` datetime DEFAULT NULL COMMENT '记录日志时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=utf8 COMMENT='操作日志记录';

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES ('58', '', '访问了getPageList方法', '2022-09-21 20:47:19');
INSERT INTO `sys_oper_log` VALUES ('59', '', '访问了updateUser方法', '2022-09-21 21:03:10');
INSERT INTO `sys_oper_log` VALUES ('60', '', '访问了getPageList方法', '2022-09-21 21:03:10');
INSERT INTO `sys_oper_log` VALUES ('61', '', '访问了getPageList方法', '2022-09-21 21:04:09');
INSERT INTO `sys_oper_log` VALUES ('62', '', '访问了getPageList方法', '2022-09-21 21:04:14');
INSERT INTO `sys_oper_log` VALUES ('63', '', '访问了getPageList方法', '2022-09-21 21:04:15');
INSERT INTO `sys_oper_log` VALUES ('64', '', '访问了getPageList方法', '2022-09-21 21:04:16');
INSERT INTO `sys_oper_log` VALUES ('65', '', '访问了getPageList方法', '2022-09-21 21:04:16');
INSERT INTO `sys_oper_log` VALUES ('66', '', '访问了getPageList方法', '2022-09-21 21:04:17');
INSERT INTO `sys_oper_log` VALUES ('67', '', '访问了getPageList方法', '2022-09-21 21:04:17');
INSERT INTO `sys_oper_log` VALUES ('68', '', '访问了getPageList方法', '2022-09-21 21:04:17');
INSERT INTO `sys_oper_log` VALUES ('69', '', '访问了saveUser方法', '2022-09-21 21:19:52');
INSERT INTO `sys_oper_log` VALUES ('70', '', '访问了getPageList方法', '2022-09-21 21:19:52');
INSERT INTO `sys_oper_log` VALUES ('71', '', '访问了getPageList方法', '2022-09-21 21:20:04');
INSERT INTO `sys_oper_log` VALUES ('72', '', '访问了saveUser方法', '2022-09-21 21:22:13');
INSERT INTO `sys_oper_log` VALUES ('73', '', '访问了getPageList方法', '2022-09-21 21:22:13');
INSERT INTO `sys_oper_log` VALUES ('74', '', '访问了saveUser方法', '2022-09-21 21:22:19');
INSERT INTO `sys_oper_log` VALUES ('75', '', '访问了getPageList方法', '2022-09-21 21:22:20');
INSERT INTO `sys_oper_log` VALUES ('76', '', '访问了batchDelete方法', '2022-09-21 21:22:26');
INSERT INTO `sys_oper_log` VALUES ('77', '', '访问了getPageList方法', '2022-09-21 21:22:26');
INSERT INTO `sys_oper_log` VALUES ('78', '', '访问了batchDelete方法', '2022-09-21 21:22:49');
INSERT INTO `sys_oper_log` VALUES ('79', '', '访问了getPageList方法', '2022-09-21 21:22:50');
INSERT INTO `sys_oper_log` VALUES ('80', '', '访问了batchDelete方法', '2022-09-21 21:24:03');
INSERT INTO `sys_oper_log` VALUES ('81', '', '访问了getPageList方法', '2022-09-21 21:24:03');
INSERT INTO `sys_oper_log` VALUES ('82', '', '访问了batchDelete方法', '2022-09-21 21:25:39');
INSERT INTO `sys_oper_log` VALUES ('83', '', '访问了getPageList方法', '2022-09-21 21:25:39');
INSERT INTO `sys_oper_log` VALUES ('84', '', '访问了getPageList方法', '2022-09-21 21:30:28');
INSERT INTO `sys_oper_log` VALUES ('85', '', '访问了deleteUser方法', '2022-09-21 21:30:33');
INSERT INTO `sys_oper_log` VALUES ('86', '', '访问了getPageList方法', '2022-09-21 21:30:33');
INSERT INTO `sys_oper_log` VALUES ('87', '', '访问了batchDelete方法', '2022-09-21 21:30:39');
INSERT INTO `sys_oper_log` VALUES ('88', '', '访问了getPageList方法', '2022-09-21 21:30:39');
INSERT INTO `sys_oper_log` VALUES ('89', '', '访问了getPageList方法', '2022-09-21 21:38:56');
INSERT INTO `sys_oper_log` VALUES ('90', '', '访问了deleteUser方法', '2022-09-21 21:39:02');
INSERT INTO `sys_oper_log` VALUES ('91', '', '访问了deleteUser方法', '2022-09-21 21:39:02');
INSERT INTO `sys_oper_log` VALUES ('92', '', '访问了getPageList方法', '2022-09-21 21:39:02');
INSERT INTO `sys_oper_log` VALUES ('93', '', '访问了getPageList方法', '2022-09-21 21:39:09');
INSERT INTO `sys_oper_log` VALUES ('94', '', '访问了getPageList方法', '2022-09-21 21:42:28');
INSERT INTO `sys_oper_log` VALUES ('95', '', '访问了getPageList方法', '2022-09-22 13:40:41');
INSERT INTO `sys_oper_log` VALUES ('96', '', '访问了login方法', '2022-09-22 13:41:01');
INSERT INTO `sys_oper_log` VALUES ('97', '', '访问了login方法', '2022-09-22 13:41:03');
INSERT INTO `sys_oper_log` VALUES ('98', '', '访问了login方法', '2022-09-22 13:45:16');
INSERT INTO `sys_oper_log` VALUES ('99', '', '访问了login方法', '2022-09-22 13:45:17');
INSERT INTO `sys_oper_log` VALUES ('100', '', '访问了login方法', '2022-09-22 13:45:50');
INSERT INTO `sys_oper_log` VALUES ('101', '', '访问了getPageList方法', '2022-09-22 13:47:24');
INSERT INTO `sys_oper_log` VALUES ('102', '', '访问了login方法', '2022-09-22 13:47:44');
INSERT INTO `sys_oper_log` VALUES ('103', '', '访问了login方法', '2022-09-22 13:50:45');
INSERT INTO `sys_oper_log` VALUES ('104', '', '访问了login方法', '2022-09-22 13:50:56');
INSERT INTO `sys_oper_log` VALUES ('105', '', '访问了login方法', '2022-09-22 13:51:29');
INSERT INTO `sys_oper_log` VALUES ('106', '', '访问了login方法', '2022-09-22 13:51:33');
INSERT INTO `sys_oper_log` VALUES ('107', '', '访问了login方法', '2022-09-22 13:55:17');
INSERT INTO `sys_oper_log` VALUES ('108', '', '访问了login方法', '2022-09-22 13:55:30');
INSERT INTO `sys_oper_log` VALUES ('109', '', '访问了login方法', '2022-09-22 13:55:30');
INSERT INTO `sys_oper_log` VALUES ('110', '', '访问了login方法', '2022-09-22 13:55:53');
INSERT INTO `sys_oper_log` VALUES ('111', '', '访问了login方法', '2022-09-22 13:55:53');
INSERT INTO `sys_oper_log` VALUES ('112', '', '访问了login方法', '2022-09-22 13:55:53');
INSERT INTO `sys_oper_log` VALUES ('113', '', '访问了login方法', '2022-09-22 13:55:53');
INSERT INTO `sys_oper_log` VALUES ('114', '', '访问了login方法', '2022-09-22 13:55:58');
INSERT INTO `sys_oper_log` VALUES ('115', '', '访问了login方法', '2022-09-22 13:55:58');
INSERT INTO `sys_oper_log` VALUES ('116', '', '访问了login方法', '2022-09-22 13:55:58');
INSERT INTO `sys_oper_log` VALUES ('117', '', '访问了login方法', '2022-09-22 13:55:58');
INSERT INTO `sys_oper_log` VALUES ('118', '', '访问了login方法', '2022-09-22 13:56:07');
INSERT INTO `sys_oper_log` VALUES ('119', '', '访问了login方法', '2022-09-22 13:56:09');
INSERT INTO `sys_oper_log` VALUES ('120', '', '访问了login方法', '2022-09-22 13:56:09');
INSERT INTO `sys_oper_log` VALUES ('121', '', '访问了login方法', '2022-09-22 13:56:12');
INSERT INTO `sys_oper_log` VALUES ('122', '', '访问了login方法', '2022-09-22 14:04:44');
INSERT INTO `sys_oper_log` VALUES ('123', '', '访问了login方法', '2022-09-22 14:05:06');
INSERT INTO `sys_oper_log` VALUES ('124', '', '访问了login方法', '2022-09-22 14:05:10');
INSERT INTO `sys_oper_log` VALUES ('125', '', '访问了login方法', '2022-09-22 14:22:42');
INSERT INTO `sys_oper_log` VALUES ('126', '', '访问了login方法', '2022-09-22 14:23:12');
INSERT INTO `sys_oper_log` VALUES ('127', '', '访问了login方法', '2022-09-22 14:23:50');
INSERT INTO `sys_oper_log` VALUES ('128', '', '访问了login方法', '2022-09-22 14:23:59');
INSERT INTO `sys_oper_log` VALUES ('129', '', '访问了login方法', '2022-09-22 14:24:04');
INSERT INTO `sys_oper_log` VALUES ('130', '', '访问了login方法', '2022-09-22 14:25:09');
INSERT INTO `sys_oper_log` VALUES ('131', '', '访问了login方法', '2022-09-22 14:25:10');
INSERT INTO `sys_oper_log` VALUES ('132', '', '访问了login方法', '2022-09-22 14:25:10');
INSERT INTO `sys_oper_log` VALUES ('133', '', '访问了login方法', '2022-09-22 14:25:11');
INSERT INTO `sys_oper_log` VALUES ('134', '', '访问了login方法', '2022-09-22 14:26:59');
INSERT INTO `sys_oper_log` VALUES ('135', '', '访问了login方法', '2022-09-22 14:27:44');
INSERT INTO `sys_oper_log` VALUES ('136', '', '访问了login方法', '2022-09-22 14:27:44');
INSERT INTO `sys_oper_log` VALUES ('137', '', '访问了login方法', '2022-09-22 14:27:44');
INSERT INTO `sys_oper_log` VALUES ('138', '', '访问了login方法', '2022-09-22 14:29:13');
INSERT INTO `sys_oper_log` VALUES ('139', '', '访问了login方法', '2022-09-22 14:29:17');
INSERT INTO `sys_oper_log` VALUES ('140', '', '访问了login方法', '2022-09-22 14:29:23');
INSERT INTO `sys_oper_log` VALUES ('141', '', '访问了login方法', '2022-09-22 14:31:30');
INSERT INTO `sys_oper_log` VALUES ('142', '', '访问了login方法', '2022-09-22 14:32:54');
INSERT INTO `sys_oper_log` VALUES ('143', '', '访问了login方法', '2022-09-22 14:33:04');
INSERT INTO `sys_oper_log` VALUES ('144', '', '访问了login方法', '2022-09-22 14:33:08');
INSERT INTO `sys_oper_log` VALUES ('145', '', '访问了login方法', '2022-09-22 14:34:24');
INSERT INTO `sys_oper_log` VALUES ('146', '', '访问了login方法', '2022-09-22 14:35:45');
INSERT INTO `sys_oper_log` VALUES ('147', '', '访问了login方法', '2022-09-22 15:10:00');
INSERT INTO `sys_oper_log` VALUES ('148', '', '访问了login方法', '2022-09-22 15:10:07');
INSERT INTO `sys_oper_log` VALUES ('149', '', '访问了login方法', '2022-09-22 15:55:46');
INSERT INTO `sys_oper_log` VALUES ('150', '', '访问了getPageList方法', '2022-09-22 15:55:55');
INSERT INTO `sys_oper_log` VALUES ('151', '', '访问了getPageList方法', '2022-09-22 15:56:00');
INSERT INTO `sys_oper_log` VALUES ('152', '', '访问了getPageList方法', '2022-09-22 15:56:04');
INSERT INTO `sys_oper_log` VALUES ('153', '', '访问了getPageList方法', '2022-09-22 15:56:15');
INSERT INTO `sys_oper_log` VALUES ('154', '', '访问了getPageList方法', '2022-09-22 16:04:05');
INSERT INTO `sys_oper_log` VALUES ('155', '', '访问了getPageList方法', '2022-09-22 16:04:08');
INSERT INTO `sys_oper_log` VALUES ('156', '', '访问了login方法', '2022-09-22 16:04:16');
INSERT INTO `sys_oper_log` VALUES ('157', '', '访问了login方法', '2022-09-22 16:04:18');
INSERT INTO `sys_oper_log` VALUES ('158', '', '访问了login方法', '2022-09-22 16:05:05');
INSERT INTO `sys_oper_log` VALUES ('159', '', '访问了login方法', '2022-09-22 16:05:06');
INSERT INTO `sys_oper_log` VALUES ('160', '', '访问了login方法', '2022-09-22 16:05:21');
INSERT INTO `sys_oper_log` VALUES ('161', '', '访问了login方法', '2022-09-22 16:05:22');
INSERT INTO `sys_oper_log` VALUES ('162', '', '访问了getPageList方法', '2022-09-22 16:05:33');
INSERT INTO `sys_oper_log` VALUES ('163', '', '访问了getPageList方法', '2022-09-22 16:06:15');
INSERT INTO `sys_oper_log` VALUES ('164', '', '访问了getPageList方法', '2022-09-22 16:06:15');
INSERT INTO `sys_oper_log` VALUES ('165', '', '访问了getPageList方法', '2022-09-22 16:06:17');
INSERT INTO `sys_oper_log` VALUES ('166', '', '访问了login方法', '2022-09-22 16:06:25');
INSERT INTO `sys_oper_log` VALUES ('167', '', '访问了getPageList方法', '2022-09-22 16:08:05');
INSERT INTO `sys_oper_log` VALUES ('168', '', '访问了login方法', '2022-09-22 16:08:16');
INSERT INTO `sys_oper_log` VALUES ('169', '', '访问了getPageList方法', '2022-09-22 16:08:22');
INSERT INTO `sys_oper_log` VALUES ('170', '', '访问了getPageList方法', '2022-09-22 16:14:54');
INSERT INTO `sys_oper_log` VALUES ('171', '', '访问了login方法', '2022-09-22 16:15:05');
INSERT INTO `sys_oper_log` VALUES ('172', '', '访问了getPageList方法', '2022-09-22 16:15:13');
INSERT INTO `sys_oper_log` VALUES ('173', '', '访问了login方法', '2022-09-22 16:20:52');
INSERT INTO `sys_oper_log` VALUES ('174', '', '访问了login方法', '2022-09-22 16:21:00');
INSERT INTO `sys_oper_log` VALUES ('175', '', '访问了login方法', '2022-09-22 16:23:26');
INSERT INTO `sys_oper_log` VALUES ('176', '', '访问了login方法', '2022-09-22 16:23:39');
INSERT INTO `sys_oper_log` VALUES ('177', '', '访问了login方法', '2022-09-22 16:26:25');
INSERT INTO `sys_oper_log` VALUES ('178', '', '访问了login方法', '2022-09-22 16:26:29');
INSERT INTO `sys_oper_log` VALUES ('179', '', '访问了login方法', '2022-09-22 16:26:29');
INSERT INTO `sys_oper_log` VALUES ('180', '', '访问了login方法', '2022-09-22 16:26:30');
INSERT INTO `sys_oper_log` VALUES ('181', '', '访问了login方法', '2022-09-22 16:26:30');
INSERT INTO `sys_oper_log` VALUES ('182', '', '访问了login方法', '2022-09-22 16:34:08');
INSERT INTO `sys_oper_log` VALUES ('183', '', '访问了login方法', '2022-09-22 16:34:25');
INSERT INTO `sys_oper_log` VALUES ('184', '', '访问了login方法', '2022-09-22 16:40:36');
INSERT INTO `sys_oper_log` VALUES ('185', '', '访问了login方法', '2022-09-22 16:40:37');
INSERT INTO `sys_oper_log` VALUES ('186', '', '访问了login方法', '2022-09-22 16:40:38');
INSERT INTO `sys_oper_log` VALUES ('187', '', '访问了login方法', '2022-09-22 16:40:38');
INSERT INTO `sys_oper_log` VALUES ('188', '', '访问了login方法', '2022-09-22 16:40:38');
INSERT INTO `sys_oper_log` VALUES ('189', '', '访问了login方法', '2022-09-22 16:41:08');
INSERT INTO `sys_oper_log` VALUES ('190', '', '访问了login方法', '2022-09-22 16:48:43');
INSERT INTO `sys_oper_log` VALUES ('191', '', '访问了login方法', '2022-09-22 16:48:57');
INSERT INTO `sys_oper_log` VALUES ('192', '', '访问了login方法', '2022-09-22 16:49:07');
INSERT INTO `sys_oper_log` VALUES ('193', '', '访问了login方法', '2022-09-22 16:49:20');
INSERT INTO `sys_oper_log` VALUES ('194', '', '访问了login方法', '2022-09-22 16:49:24');
INSERT INTO `sys_oper_log` VALUES ('195', '', '访问了login方法', '2022-09-22 16:49:33');
INSERT INTO `sys_oper_log` VALUES ('196', '', '访问了login方法', '2022-09-22 16:49:42');
INSERT INTO `sys_oper_log` VALUES ('197', '', '访问了login方法', '2022-09-22 16:51:00');
INSERT INTO `sys_oper_log` VALUES ('198', '', '访问了login方法', '2022-09-22 16:51:06');
INSERT INTO `sys_oper_log` VALUES ('199', '', '访问了login方法', '2022-09-22 16:52:53');
INSERT INTO `sys_oper_log` VALUES ('200', '', '访问了login方法', '2022-09-22 16:55:01');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `user_name` varchar(30) NOT NULL COMMENT '用户账号',
  `real_name` varchar(10) DEFAULT NULL COMMENT '真实姓名',
  `email` varchar(50) DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) DEFAULT '' COMMENT '手机号码',
  `avatar` varchar(300) DEFAULT '' COMMENT '头像地址',
  `password` varchar(100) DEFAULT NULL COMMENT '密码',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `un_user_name` (`user_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='用户信息表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('12', 'admin', '吴大胖', '1134135987@qq.com', '18086295423', '', 'e10adc3949ba59abbe56e057f20f883e', '超级管理员账号');
